var express = require('express');
var router = express.Router();
var Product = require('../models/product');

/* GET products listing. */
router.get('/:items?/:offset?', async (req, res, next) => {

  let search = req.query.s ? req.query.s : '';
  let take = req.params.items ? req.params.items : 10;
  let offset = (req.params.offset > 1) ? req.params.offset : 0;

  let products = await Product
    .query( qb => qb.whereRaw(`LOWER(name) LIKE ?`, [`%${search.toLowerCase()}%`]) )
    .fetchPage({
      withRelated: ['categories', 'images'],
      limit: take,
      offset: offset
    });

  res.json({
    search: search,
    take: products.pagination.limit,
    offset: products.pagination.offset,
    total: products.pagination.rowCount,
    products: products.models,
  });

});

module.exports = router;
