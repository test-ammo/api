require('dotenv').config();

module.exports = {

  testing: {
    client: 'sqlite3',
    connection: {
      filename: './dev.sqlite3'
    },
    seeds: {
      directory: './database/seeds/dev'
    },
    migrations: {
      directory: './database/migrations'
    }
  },

  development: {
    client: process.env.DB_CLIENT ? process.env.DB_CLIENT : 'mysql',
    connection: {
      host : process.env.DB_HOST,
      database: process.env.DB_DATABASE,
      user:     process.env.DB_USERNAME,
      password: process.env.DB_PASS
    },
    seeds: {
      directory: './database/seeds/dev'
    },
    migrations: {
      directory: './database/migrations'
    }
  },

  staging: {
    client: process.env.DB_CLIENT ? process.env.DB_CLIENT : 'mysql',
    connection: {
      host : process.env.DB_HOST,
      database: process.env.DB_DATABASE,
      user:     process.env.DB_USERNAME,
      password: process.env.DB_PASS
    },
    migrations: {
      directory: './database/migrations',
      tableName: 'migrations'
    },
    seeds: {
      directory: './database/seeds/dev'
    }
  },

  production: {
    client: process.env.DB_CLIENT ? process.env.DB_CLIENT : 'mysql',
    connection: {
      host: process.env.DB_HOST,
      database: process.env.DB_DATABASE,
      user:     process.env.DB_USERNAME,
      password: process.env.DB_PASS
    },
    migrations: {
      directory: './database/migrations',
      tableName: 'migrations'
    }
  }

};
