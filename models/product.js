var bookshelf = require('../bookshelf');
var Category = require('./category');
var Image = require('./image');


module.exports = bookshelf.Model.extend({
  tableName: 'products',
  categories: function() {
    return this.belongsToMany(Category, 'product_has_categories', 'productid', 'categoryid');
  },
  images: function() {
    return this.belongsToMany(Image, 'product_has_images', 'productid', 'imageid');
  }
});