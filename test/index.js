var app = require("../app");
var supertest = require("supertest")(app);

it("Responds with 'AMMO API!'", (done) => {
    supertest.get("/")
        .expect(200)
        .expect({
            title: "AMMO API"
        }).end( () => done() );
});