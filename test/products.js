var app = require('../app');
var supertest = require('supertest')(app);
var should = require('chai').should();
var expect = require('chai').expect;
var knex = require('../knex');
var mock = require('../database/factories/product');

describe('Products API - Pagination', async () => {

  before('migrations/seed', async () => {
    await knex.migrate.latest();
    await knex.seed.run().then();
  });

  after('rollback', async () => {
    await knex.migrate.rollback();
  });

  it('Testing without pagination', (done) => {
    supertest.get('/products')
      .expect(200)
      .expect('Content-Type', /json/)
      .end((err, res) => {

        res.body.take.should.equal(10);
        res.body.offset.should.equal(0);

        res.body.should.have.property('products').with.lengthOf(10);

        done();
      });
  });

  it('Testing with take parameter', (done) => {
    supertest.get('/products/50')
      .expect(200)
      .expect('Content-Type', /json/)
      .end((err, res) => {

        res.body.take.should.equal(50);
        res.body.offset.should.equal(0);
        res.body.should.have.property('products').with.lengthOf(50);

        done();
      });
  });


  it('Testing with full pagination', (done) => {
    supertest.get('/products/50/50')
      .expect(200)
      .expect('Content-Type', /json/)
      .end((err, res) => {

        res.body.take.should.equal(50);
        res.body.offset.should.equal(50);
        res.body.should.have.property('products').with.lengthOf(50);

        done();
      });
  });

  it('Testing with search term', async () => {

    let product = mock();
    await product.save();
    product = product.toJSON();
    let search = product.name.replace(/ .*/,'');

    let response = await supertest.get('/products/1/0?s=' + search).expect(200).expect('Content-Type', /json/);
    response.body.search.should.equal(search);

    response.body.total.should.gt(1);

    for(let p in response.body.products) {
      response.body.products[p].name.should.match(new RegExp('^.*?' + search + '.*?$') );
    }

  });

});