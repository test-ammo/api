# API

## Requer Node 9

Passos para instalação:
  1. `npm install`
  2. Configurar arquivo `.env` (Usar .env.example)
  3. `npm run migrate`
  4. `npm run knex seed:run`
  5. `npm start` OU `npm run dev`