var knex = require('./knex');
var bookshelf = require('bookshelf')(knex)

bookshelf.plugin('pagination')

module.exports = bookshelf;