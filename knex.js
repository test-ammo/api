const knex = require('knex');
const config = require('./knexfile');
var env = config[process.env.NODE_ENV] ? config[process.env.NODE_ENV] : config['development'];

module.exports = knex(env);