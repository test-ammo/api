
exports.up = (knex, Promise) => {

  return Promise.all([
    knex.schema.createTableIfNotExists('categories', (table) => {
      table.increments();
      table.string('name').notNullable();
      table.string('slug').notNullable();
      table.text('description');
      table.integer('parent').unsigned().nullable();
      table.foreign('parent').references('categories.id');
      table.timestamps();
    }),

    knex.schema.createTableIfNotExists('product_has_categories', (table) => {
      table.integer('productid').unsigned();
      table.integer('categoryid').unsigned();
      table.foreign('categoryid').references('categories.id');
      table.foreign('productid').references('products.id');
    })

  ]);

};

exports.down = (knex, Promise) => {

  return Promise.all([
    knex.schema.dropTableIfExists('product_has_categories'),
    knex.schema.dropTableIfExists('categories')
  ]);

};