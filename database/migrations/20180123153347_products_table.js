
exports.up = (knex, Promise) => {

  return Promise.all([
    knex.schema.createTableIfNotExists('products', (table) => {
      table.increments();
      table.string('name').notNullable();
      table.string('sku').notNullable().unique();
      table.decimal('price').notNullable().default(0);
      table.decimal('promotional_price');
      table.text('description');
      table.timestamps();
    })
  ]);

};

exports.down = (knex, Promise) => {

  return Promise.all([
    knex.schema.dropTableIfExists('products')
  ]);

};
