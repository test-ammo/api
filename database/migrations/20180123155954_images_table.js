
exports.up = (knex, Promise) => {

    return Promise.all([
      knex.schema.createTableIfNotExists('images', (table) => {
        table.increments();
        table.string('name').notNullable();
        table.string('path').notNullable();
        table.timestamps();
      }),
  
      knex.schema.createTableIfNotExists('product_has_images', (table) => {
        table.integer('productid').unsigned();
        table.integer('imageid').unsigned();
        table.foreign('imageid').references('images.id');
        table.foreign('productid').references('products.id');
      })
  
    ]);
  
  };
  
  exports.down = (knex, Promise) => {
  
    return Promise.all([
      knex.schema.dropTableIfExists('product_has_images'),
      knex.schema.dropTableIfExists('images')
    ]);
  
  };