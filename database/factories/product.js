const faker = require('faker');
const Product = require('./../../models/product');

module.exports = () => {
  return new Product({
    name: faker.commerce.productName(),
    sku: faker.finance.iban(),
    price: faker.random.number(),
    promotional_price: faker.random.number(),
    description: faker.lorem.text(),
  });
};