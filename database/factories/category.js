const faker = require('faker');
const Category = require('./../../models/category');

function slugify(text)
{
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^\w\-]+/g, '')
    .replace(/\-\-+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '');
}

module.exports = () => {
  let name = faker.commerce.productMaterial();
  return new Category({
    name: name,
    slug: slugify(name),
  });
};