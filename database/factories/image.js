const faker = require('faker');
const Image = require('./../../models/image');

module.exports = () => {
  return new Image({
    name: faker.image.image(),
    path: faker.image.imageUrl()
  });
};