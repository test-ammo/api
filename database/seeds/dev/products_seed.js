const faker = require('faker');
const mock  = require('../../factories/product');
const mockCategory = require('../../factories/category');
const mockImage = require('../../factories/image');

exports.seed = async (knex, Promise) => {

  for(let i=0; i<250; i++) {
    let product = await mock().save();

    await product.categories().attach( await mockCategory().save() );
    await product.categories().attach( await mockCategory().save() );

    await product.images().attach( await mockImage().save() );
    await product.images().attach( await mockImage().save() );
  }

  return Promise.all([]);

};
